package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.*;

public class Calculator {
    private static final Set<Character> operatorSet = new HashSet<>(Arrays.asList('+', '-', '*', '/', '(', ')'));

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        List<String> tokens = parse(statement);
        List<String> rpn = rpn(tokens);
        if (rpn == null) {
            return null;
        }
        Double result = calculate(rpn);
        if (result == null) {
            return null;
        }
        result = Math.round(result * 10000) / 10000.0;
        if (Math.round(result) == result) {
            return String.valueOf(Math.round(result));
        }
        return result.toString();
    }

    private double apply(double a, double b, String operator) {
        if (operator.equals("+")) {
            return a + b;
        }
        if (operator.equals("-")) {
            return a - b;
        }
        if (operator.equals("*")) {
            return a * b;
        }
        if (operator.equals("/")) {
            return a / b;
        }
        throw new IllegalArgumentException();
    }

    private Double calculate(List<String> list) {
        Stack<Double> stack = new Stack<>();
        for (String token : list) {
            if (!operatorSet.contains(token.charAt(0))) {
                try {
                    stack.push(Double.parseDouble(token));
                } catch (NumberFormatException e) {
                    return null;
                }
            } else {
                if (stack.size() < 2) {
                    return null;
                }
                double previous = stack.pop();
                double preprevious = stack.pop();
                if (previous != 0) {
                    stack.push(apply(preprevious, previous, token));
                } else {
                    return null;
                }
            }
        }
        if (stack.size() == 1) {
            return stack.pop();
        }
        return null;
    }

    private int precedence(String s) {
        switch (s) {
            case "+":
                return 1;
            case "-":
                return 1;
            case "*":
                return 2;
            case "/":
                return 2;
            default:
                throw new IllegalArgumentException();
        }
    }

    private List<String> rpn(List<String> tokens) {
        List<String> result = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        for (String token : tokens) {
            if (!operatorSet.contains(token.charAt(0))) {
                result.add(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.isEmpty() && !stack.peek().equals("(")) {
                    result.add(stack.pop());
                }
                if (stack.isEmpty()) {
                    return null;
                }
                stack.pop();
            } else {
                while (true) {
                    if (stack.isEmpty()) {
                        break;
                    }
                    String o2 = stack.peek();
                    if (o2.equals("(") || o2.equals(")")) {
                        break;
                    }
                    if (precedence(token) <= precedence(o2)) {
                        stack.pop();
                        result.add(o2);
                    } else break;
                }
                stack.push(token);
            }
        }
        while (!stack.isEmpty()) {
            if (stack.peek().equals("(") || stack.peek().equals(")")) {
                return null;
            }
            result.add(stack.pop());
        }
        return result;
    }

    private List<String> parse(String string) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < string.length(); ) {
            if (operatorSet.contains(string.charAt(i))) {
                result.add(string.substring(i, i + 1));
                i++;
            } else {
                int temp = i;
                while (temp < string.length() && !operatorSet.contains(string.charAt(temp))) {
                    temp++;
                }
                result.add(string.substring(i, temp));
                i = temp;
            }
        }
        return result;
    }
}
