package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (targetFile == null || sourceFile == null) {
            throw new IllegalArgumentException();
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(sourceFile));
            String line;
            HashMap<String, Long> hashMap = new HashMap<>();
            while ((line = bufferedReader.readLine()) != null) {
                if (hashMap.containsKey(line)) {
                    hashMap.put(line, hashMap.get(line) + 1);
                } else {
                    hashMap.put(line, (long) 1);
                }
            }
            ArrayList<String> list = new ArrayList<>(hashMap.keySet());
            Collections.sort(list);
            PrintWriter printWriter = new PrintWriter(new FileWriter(targetFile, true));

            for (String currentLine : list) {
                printWriter.append(currentLine).append("[").append(hashMap.get(currentLine).toString()).append("]\n");
            }
            printWriter.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}
